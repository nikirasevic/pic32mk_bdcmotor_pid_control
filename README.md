# Overview

This project was developed 

# GUI Installation

To use python GUI developed for PID tuning you need to install some programs and packages on your PC.

First you'll need to have python3 installed.

## Install pip

on Windows:

```bash
python -m pip install -U pip
``` 

on Linux:

```bash
pip install -U pip
```

## Install PyQt5:

Graphical interface is written in python using qt5, so you need to install also PyQt5:

```bash
pip install PyQt5
```

## Install PyQtGraph

```bash
pip install pyqtgraph
```


## Install PySerial

```bash
pip install pyserial
```

If you are getting error that module serial doesnt exist use pip3 for installation:

```bash
pip3 install pyserial
```

# GUI overview

## PID Controller Tunning

The graph bellow shows the angular velocity in RPM-s over time, with P regulation implemented.

![P controll](https://gitlab.com/nikirasevic/pic32mk_bdcmotor_pid_control/-/raw/master/docs/img/pid-response_p.png)


Response of the full PID controller during parameter tuning is depicted bellow.

![PID controll](https://gitlab.com/nikirasevic/pic32mk_bdcmotor_pid_control/-/raw/master/docs/img/pid-response_pid.png)


Final PID controller with tuned parameters is shown on the following graph.

![PID controll](https://gitlab.com/nikirasevic/pic32mk_bdcmotor_pid_control/-/raw/master/docs/img/pid-response.png)
